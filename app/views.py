from flask import render_template, url_for, Response
from app import app
import cv2
import time

def gen_frames(source):  # generate frame by frame from camera
    camera = cv2.VideoCapture(source['stream'])
    try:
        framerate = source['framerate']
    except KeyError :
        framerate = 0.2
    try:
        width = source['width']
    except KeyError :
        width = 300
    try:
        height = source['height']
    except KeyError :
        height = 180
    prev = 0
    while True:
        time_elapsed = time.time() - prev
        # Capture frame-by-frame
        success, frame = camera.read()  # read the camera frame
        if not success:
            break
        else:
            if  time_elapsed > 1./framerate :
                prev = time.time()
                frame_r = cv2.resize(frame,(width,height), interpolation = cv2.INTER_AREA)
                ret, buffer = cv2.imencode('.jpg', frame_r)
                frame = buffer.tobytes()
                yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result

@app.route('/')
@app.route('/index')
def index():
    """Video streaming home page."""
    sources_conf = app.config['SOURCES_DEFINITIONS']
    for s in sources_conf:
        s['stream_url'] = url_for('stream',id=s['id'])    
    return render_template('index.html', sources=sources_conf)

@app.route('/stream/<path:id>')
def stream(id):
    """Video streaming route. Put this in the src attribute of an img tag."""
    for source in app.config['SOURCES_DEFINITIONS'] :
        if source['id'] == id :
            return Response(gen_frames(source),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
