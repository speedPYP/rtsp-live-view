from flask import Flask, render_template, Response
import logging
from logging.handlers import RotatingFileHandler
import cv2
import time

app = Flask(__name__)

#Configuration definition
app.config.from_pyfile('configuration/config.ini')

#Logging definition
handler = RotatingFileHandler(app.config['LOG_FILE'], backupCount=1)
formatter = logging.Formatter( "%(asctime)s | %(pathname)s:%(lineno)d | %(funcName)s | %(levelname)s | %(message)s ")
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)
app.logger.addHandler(handler)

#TODO: only 1 connection to video and only if necessary
#      watch clients connected.

#Log application configuration
for key in app.config:
    app.logger.debug(key+"="+str(app.config[key]))

from app import views


